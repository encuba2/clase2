﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital
{
    class CuartoHospital
    {


        // Atributos

        private String nombre;
        private int numeroPiso;
        private int cantidaPacientes;


        //Metodos

        public string ingresarCuarto()
        {

            // Validaciones 

            if (nombre == null)
            {
                return "Por favor ingrese el nombre del cuarto";
            }


            if (numeroPiso < 1)
            {
                return "Por favor ingrese el numero de piso del cuarto";

            }

            // Logica

            return "Se ingreso el cuarto: " + nombre + " esta en el piso " + numeroPiso;

         }

        // Gets and Setter


        public String Nombre
        {
            get { return nombre; }
            set { nombre = value; }
        }


        public int NumeroPiso
        {
            get { return numeroPiso; }
            set { numeroPiso = value; }
        }

        public int CantidaPacientes
        {
            get { return cantidaPacientes; }
            set { cantidaPacientes = value; }
        }

    }
}
